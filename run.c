#include <mpi.h>
#include <stdio.h>
#include <sys/time.h>


#define ROW_SIZE 16384
#define EPSILON  0.1

float fabs(float f);
double When();

void main(int argc, char *argv[])
{
    float *nA, *oA, *tmp;
    int i, done, reallydone;
    int cnt;
    int start, end;
    int numCells;

    double starttime;

    int nproc, iproc;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    starttime = When();

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
    // fprintf(stderr,"%d: Hello from %d of %d\n", iproc, iproc, nproc);
    
    /* Determine how much I should be doing and allocate the arrays*/
    numCells = (ROW_SIZE * ROW_SIZE) / nproc;
    nA = (float *)malloc((numCells + 2 * ROW_SIZE) * sizeof(float));
    oA = (float *)malloc((numCells + 2 * ROW_SIZE) * sizeof(float));

    start = ROW_SIZE;
    end = start + numCells;

    /* Initialize the cells */
    for (i = 0; i < numCells + 2 * ROW_SIZE; i++)
    {
        // set boundary cells to 0
        if (i % ROW_SIZE == 0 || i % ROW_SIZE == ROW_SIZE - 1) {
            nA[i] = oA[i] = 0;    
        } else {
            nA[i] = oA[i] = 50;
        }
    }

    /* Initialize the Boundaries */
    if (iproc == 0) //set the bottom row to 100
    {
        start += ROW_SIZE;
        for (i = start; i < start + ROW_SIZE; i++)
        {
             nA[i] = oA[i] = 100;
        }
    }
    if (iproc == nproc - 1) // set the highest row to 0
    {
        end -= ROW_SIZE;
        for (i = numCells; i < numCells + ROW_SIZE; i++)
        {
            nA[i] = oA[i] = 0;
        }
    }



    /* Now run the relaxation */
    reallydone = 0;
    for(cnt = 0; !reallydone; cnt++)
    {
        /* First, I must get my neighbors boundary values */
        if (iproc != 0) //if I'm not the first guy
        {
            if (iproc % 2 != 0) { //Odd num processors receive first
                MPI_Recv(&oA[0], ROW_SIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD, &status);
            }
            MPI_Send(&oA[ROW_SIZE], ROW_SIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD);
            if (iproc % 2 == 0) {
                MPI_Recv(&oA[0], ROW_SIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD, &status);
            }
        }

        if (iproc != nproc - 1) //if I'm not the last guy
        {
            if (iproc % 2 != 0) {
                MPI_Recv(&oA[numCells + ROW_SIZE], ROW_SIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD, &status);
            }
            MPI_Send(&oA[numCells], ROW_SIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD);
            if (iproc % 2 == 0) {
                MPI_Recv(&oA[numCells + ROW_SIZE], ROW_SIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD, &status);
            }
        }

        /* Do the calculations */
        for (i = start; i < end; i++)
        {
            if (i % ROW_SIZE == 0 || i % ROW_SIZE == ROW_SIZE - 1) {
                continue;    
            } else {
                nA[i] = (oA[i-1] + oA[i+1] + oA[i+ROW_SIZE] + oA[i-ROW_SIZE] + 4 * oA[i]) / 8.0;
            }
        }

        /* Check to see if we are done */
        done = 1;
        for (i = start; i < end; i++)
        {
            if (i % ROW_SIZE == 0 || i % ROW_SIZE == ROW_SIZE - 1) {
                continue;    
            }

            if (fabs((nA[i-1] + nA[i+1] + nA[i+ROW_SIZE] + nA[i-ROW_SIZE])/ 4.0 - nA[i]) > EPSILON)
            {
                done = 0;
                break;
            }
        }
        
        /* Do a reduce to see if everybody is done */
        MPI_Allreduce(&done, &reallydone, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);

        /* Swap the pointers */
        tmp = nA;
        nA = oA;
        oA = tmp;
    }

    /* print out the number of iterations to relax */
    if (iproc == 0) {
        fprintf(stderr, "%d:It took %d iterations and %lf seconds to relax the system\n", 
                                   iproc,cnt,When() - starttime);
    }
    MPI_Finalize();
}
    
float fabs(float f)
{
    return (f > 0.0)? f : -f ;
}

/* Return the correct time in seconds, using a double precision number.       */
double
When()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}